{ lib
, kernel
, stdenv
, fetchFromGitHub
}:

stdenv.mkDerivation rec {
  pname = "btusb-kernel-module";
  version = "0.8-custom";

  src = kernel.src;

  patches = [ ./btusb.c.patch ];

  nativeBuildInputs = kernel.moduleBuildDependencies;

  postPatch = ''
    export sourceRoot=$(pwd)/drivers/bluetooth
    echo 'obj-m = btusb.o' > $(pwd)/drivers/bluetooth/Makefile
  '';

  makeFlags = kernel.makeFlags ++ [
    "-C"
    "${kernel.dev}/lib/modules/${kernel.modDirVersion}/build"
    "M=$(sourceRoot)"
    "-j8"
  ];

  buildFlags = [ "modules" ];
  installFlags = [ "INSTALL_MOD_PATH=${placeholder "out"}" ];
  #  installFlags = [ "INSTALL_MOD_PATH=${kernel}/lib/modules/${kernel.modDirVersion}/updates" ];
  installTargets = [ "modules_install" ];

  meta = with lib; {
    description = "btusb driver fix for this laptop";
    platforms = platforms.linux;
  };
}
