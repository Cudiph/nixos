{ config, pkgs, ... }:

let
  btusb-kernel-module = pkgs.callPackage ./btusb.nix {
    kernel = config.boot.kernelPackages.kernel;
  };
in
{
  boot.extraModulePackages = [
    btusb-kernel-module
  ];

  boot.kernelModules = [ "btusb" ];

  fileSystems =
    let
      ntfsDrive = {
        fsType = "ntfs-3g";
        options = [ "rw" "uid=1000" "umask=022" "nofail" ];
      };
    in
    {
      "/d" = (ntfsDrive // { device = "/dev/disk/by-uuid/646491DD6491B1F2"; });
      "/e" = (ntfsDrive // { device = "/dev/disk/by-uuid/4EE20B08E20AF44D"; });
      "/f" = (ntfsDrive // { device = "/dev/disk/by-uuid/2486407F86405408"; });
      "/g" = (ntfsDrive // { device = "/dev/disk/by-uuid/580865A30865813C"; });
      "/h" = (ntfsDrive // { device = "/dev/disk/by-uuid/FA5A6C945A6C4F85"; });
    };

  hardware.nvidia = {
    modesetting.enable = true;
    powerManagement.enable = true;
    nvidiaSettings = true;

    prime = {
      offload = {
        enable = true;
        enableOffloadCmd = true;
      };

      amdgpuBusId = "PCI:1:0:0";
      nvidiaBusId = "PCI:5:0:0";
    };
  };

  services.xserver = {
    videoDrivers = [ "amdgpu" ];
    deviceSection = ''
      	Option "TearFree" "true"
      	Option "VariableRefresh" "true"
    '';

  };

  systemd.services."battery-threshold" = {
    description = "change battery charging threshold to 80%";
    script = ''
      echo 80 | tee /sys/class/power_supply/BAT0/charge_control_end_threshold
    '';
    wantedBy = [ "multi-user.target" ];
  };
}

