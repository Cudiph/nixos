# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running `nixos-help`).

{ lib, config, pkgs, ... }:

{
  imports =
    [
      # Include the results of the hardware scan.
      ./hardware-configuration.nix
      ./asus/asus-tuf.nix
    ];

  nixpkgs.config.allowUnfree = true;
  nix.settings.experimental-features = [ "nix-command" "flakes" ];

  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot = {
    enable = true;
    consoleMode = "max";
    extraEntries."archgrub.conf" = ''
      title "Arch Linux"
      efi /EFI/GRUB/grubx64.efi
    '';
  };
  boot.loader.efi.canTouchEfiVariables = true;
  boot.supportedFilesystems = [ "ntfs" ];

  networking.hostName = "nixos"; # Define your hostname.
  networking.networkmanager.enable = true; # Easiest to use and most distros use this by default.

  # Set your time zone.
  time.timeZone = "Asia/Jakarta";
  time.hardwareClockInLocalTime = true;

  # Select internationalisation properties.
  i18n.defaultLocale = "en_US.UTF-8";
  # console = {
  #   font = "Lat2-Terminus16";
  #   keyMap = "us";
  #   useXkbConfig = true; # use xkbOptions in tty.
  # };

  # Enable the X11 windowing system.
  services.xserver = {
    enable = true;
    layout = "us";
    # xkbOptions = "eurosign:e,caps:escape";

    displayManager.sddm.enable = true;
    windowManager.awesome =
      with pkgs;
      let
        luaEnv = pkgs.lua.withPackages (ps: [ ps.lgi ps.ldoc ]);
      in
      {
        enable = true;
        package = pkgs.awesome.overrideAttrs (old: {
          version = "master";
          src = pkgs.fetchFromGitHub {
            owner = "awesomeWM";
            repo = "awesome";
            rev = "aa8c7c6e27a20fa265d3f06c5dc3fe72cc5f021e";
            sha256 = "DGAImB4u8sRP9PEoZ4YXAxopa8eaJ7YJxSiBh36yfaE=";
          };
          patches = [ ];
          postInstall = ''
            	  # Don't use wrapProgram or the wrapper will duplicate the --search
            	  # arguments every restart
            	  mv "$out/bin/awesome" "$out/bin/.awesome-wrapped"
            	  makeWrapper "$out/bin/.awesome-wrapped" "$out/bin/awesome" \
            	    --set GDK_PIXBUF_MODULE_FILE "$GDK_PIXBUF_MODULE_FILE" \
            	    --add-flags '--search ${luaEnv}/lib/lua/${lua.luaversion}' \
            	    --add-flags '--search ${luaEnv}/share/lua/${lua.luaversion}' \
            	    --prefix GI_TYPELIB_PATH : "${lib.makeSearchPath "lib/girepository-1.0" [ pango.out playerctl harfbuzz.out gdk-pixbuf.out librsvg.out ]}"

            	  wrapProgram $out/bin/awesome-client \
            	    --prefix PATH : "${which}/bin"
            	'';
          postPatch = ''
                      patchShebangs tests/examples/_postprocess.lua
            	'';
        });
      };

    libinput = {
      enable = true;
      mouse.accelProfile = "flat";
      mouse.accelSpeed = "-0.75"; # for 2400 dpi
      touchpad.accelProfile = "flat";
    };
  };

  # bluetooth
  hardware.bluetooth.enable = true;
  services.blueman.enable = true;

  # Pipewire.
  sound.enable = true;
  security.rtkit.enable = true;
  services.pipewire = {
    enable = true;
    alsa.enable = true;
    alsa.support32Bit = true;
    pulse.enable = true;
    jack.enable = true;
  };

  # fonts
  fonts.fonts = with pkgs; [
    noto-fonts
    noto-fonts-cjk
    noto-fonts-emoji
    corefonts

    (nerdfonts.override {
      fonts = [
        "FiraCode"
        "JetBrainsMono"
        "RobotoMono"
      ];
    })
  ];

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.cudiph = {
    isNormalUser = true;
    extraGroups = [ "wheel" "networkmanager" ];
    uid = 1000;
    shell = pkgs.zsh;
  };

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
    # dev
    git
    gcc
    gnumake
    (python3.withPackages (ps: with ps; [
      pip
      requests
      pwntools
      z3
    ]))
    lua
    nodejs
    pkgconfig
    shellcheck
    gdb

    # utility
    brightnessctl
    neofetch
    neovim
    vim
    du-dust
    ripgrep
    fd
    wget
    htop
    zip
    unzip
    file
    pwndbg
    ltrace
    patchelf
    usbutils
    p7zip
    fzf
    yt-dlp
    lsof

    # desktop ui/ux
    waybar
    swayidle
    dunst
    rofi-wayland
    slurp
    grim
    eww
    unclutter-xfixes
    picom-jonaburg
    redshift
    networkmanagerapplet
    wl-clipboard
    adwaita-qt
    (catppuccin-gtk.override {
      accents = [ "green" ];
      variant = "mocha";
    })
    papirus-icon-theme
    lxappearance
    libsForQt5.qt5ct
    onboard

    # gaming
    steam

    # gui apps
    alacritty
    firefox
    flameshot
    gparted
    kitty
    lxqt.pcmanfm-qt
    copyq
    webcord
    qbittorrent
    xarchiver
    telegram-desktop
    evince
    notepadqq

    # multimedia
    gimp
    feh
    pavucontrol
    quodlibet-full
    mpv
    ffmpeg
    ffmpegthumbnailer
    playerctl
    pulseaudio # just to get pactl
    zathura
  ];

  environment.variables = {
    EDITOR = "nvim";
    VISUAL = "nvim";
    QT_QPA_PLATFORMTHEME = "qt5ct";
  };

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  # programs.gnupg.agent = {
  #   enable = true;
  #   enableSSHSupport = true;
  # };
  programs = {
    zsh.enable = true;
    hyprland.enable = true;
  };

  # List services that you want to enable:
  services.openssh.enable = true;

  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  networking.firewall.enable = false;

  system.activationScripts = {
    ldso = ''
      mkdir -p /lib64
      ln -sf ${pkgs.glibc.out}/lib64/ld-linux-x86-64.so.2 /lib64/ld-linux-x86-64.so.2
    '';
  };

  documentation.dev.enable = true;

  # Copy the NixOS configuration file and link it from the resulting system
  # (/run/current-system/configuration.nix). This is useful in case you
  # accidentally delete configuration.nix.
  system.copySystemConfiguration = true;

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It's perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "23.05"; # Did you read the comment?

}

